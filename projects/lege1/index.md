---
title: 'Lege1'
image: './lege1-1.png'
---

[Lege1](https://lege1.no) is a website for a local medical clinic. The site is made with Wordpress to give the owners control over its content and ensure maintainability in the long term.
CDN caching is done by Cloudfront, and internal cache is done by a Wordpress optimize plugin and by memcached on the hosting server. 
SEO and presence on Google Maps and Facebook is ensured as this is crucial for a local business website.

|                |                                                |
| -------------- | ---------------------------------------------- |
| Status         | Soon published.                      |
| Problems       | Seamless integration with booking system.                     |
| Frontend stack | Wordpress                                       |
| Backend stack  | Wordpress, Siteground |
