---
title: 'Alle byggpriser'
image: './allebyggpriser-1.png'
image2: './allebyggpriser-2.png'
image3: './allebyggpriser-3.png'
order: 117
---

[allebyggpriser.no](https://allebyggpriser.no) is very similar to allematpriser, just for building materials. This project uses parts of the same backend engine. Some changes have been made in categorization and search, as there are notable differences between the grocery sector and building materials sector.

|                |                                                |
| -------------- | ---------------------------------------------- |
| Status         | MVP shipped.                                   |
| Problems       | Needs promotion, better UX, and user feedback. |
| Frontend stack | Vue                                            |
| Backend stack  | Scrapy, AWS Lambda, Strapi, Express, MongoDB   |
| Other tech     | AWS                                   |
